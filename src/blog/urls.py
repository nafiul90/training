from django.contrib import admin
from django.urls import path,include
from .views import PostListView, PostDetailView,PostUpdateView,PostDeleteView,PostCreateView,LoginViewSet
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register('login',LoginViewSet,base_name='login')

urlpatterns = [
    path('', PostListView.as_view(), name = 'list' ),
    path('<int:pk>',PostDetailView.as_view(),name = 'post'),
    path('<int:pk>/update',PostUpdateView.as_view(),name='update-post'),
    path('<int:pk>/delete',PostDeleteView.as_view(),name = 'delete-post'),
    path('<create>',PostCreateView.as_view(),name = 'create'),
    path('api/',include(router.urls))

]