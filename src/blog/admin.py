from django.contrib import admin
from .models import Post

admin.site.site_header = "Blog admin panel"

class PostAdmin(admin.ModelAdmin):
    list_display = ('title', 'date')
    list_filter = ('date',)
    search_fields = ('date',)


admin.site.register(Post,PostAdmin)
