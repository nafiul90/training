from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Post(models.Model):
    author = models.ForeignKey(User,on_delete=models.CASCADE)
    title = models.CharField(max_length=200,null = True)
    content = models.TextField(max_length=500,null = True)
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.title) + " " + str(self.author)


