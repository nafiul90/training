from django.shortcuts import render
from rest_framework import generics
from .models import Post
from .serializers import PostSerializers
from rest_framework import status
from rest_framework.authentication import TokenAuthentication
from rest_framework import permissions
from rest_framework.authtoken.serializers import  AuthTokenSerializer
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework import viewsets
# Create your views here.


class PostListView(generics.ListAPIView):
    queryset = Post.objects.all()
    serializer_class = PostSerializers
    permission_classes = (permissions.IsAuthenticated,)
    authentication_classes = (TokenAuthentication,)

    def perform_authentication(self, request):
        print(request.user)

        # self.permission_classes = (permissions.IsAuthenticated,)
        # self.authentication_classes = (TokenAuthentication,)

    # def permission_denied(self, request, message=None):
    #     print("permiiton denied")




class PostDetailView(generics.RetrieveAPIView):
    queryset = Post.objects.all()
    serializer_class = PostSerializers
    permission_classes = (permissions.IsAuthenticated,)
    authentication_classes = (TokenAuthentication,)



class PostDeleteView(generics.DestroyAPIView):
    queryset = Post.objects.all()
    serializer_class = PostSerializers



class PostUpdateView(generics.UpdateAPIView):
    queryset = Post.objects.all()
    serializer_class = PostSerializers



    #curl -X POST http://127.0.0.1:8000/blog/api/login/ "Authoprization: Token b2257618aa6c12145f8a261293c07efaeb6e5d6a"

class PostCreateView(generics.CreateAPIView):
    queryset = Post.objects.all()
    serializer_class = PostSerializers
    authentication_classes = (TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)



class LoginViewSet(viewsets.ViewSet):
    """checks email and password"""

    serializer_class = AuthTokenSerializer

    def create(self,request):
        #"""use the obtainAuthToken APIView to validate and create a token"""

        return ObtainAuthToken().post(request)
